import React from "react";
import { Box, Typography } from "@material-ui/core";
import ListView from "./ListView";

const GridView = () => {
  return (
    <Box width="400px" bgcolor="white" p="16px" mx="auto">
      <Typography variant="h5">Users</Typography>
      <Box display="flex" p="16px" justifyContent="center">
        <ListView />
        <ListView />
      </Box>
      <Box display="flex" p="16px" justifyContent="center">
        <ListView />
        <ListView />
      </Box>
    </Box>
  );
};

export default GridView;
