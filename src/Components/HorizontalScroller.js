import React from 'react'
import { Box, Typography } from '@material-ui/core'
import ListView from './ListView'


const HorizontalScroller = () => {
    return (
        <Box bgcolor="white" p="16px">
        <Typography variant="h5">Users</Typography>
        <Box display="flex" overflow="auto">
            <ListView/>
            <ListView/>
            <ListView/>
            <ListView/>
            <ListView/>
            <ListView/>
            <ListView/>
        </Box>
        </Box>
    ) ;
}
 
export default HorizontalScroller
