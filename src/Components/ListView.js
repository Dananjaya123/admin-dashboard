import React from 'react'
import { Box, Typography } from '@material-ui/core'
import { green, grey } from '@material-ui/core/colors'


const ListView = () => {
    return (
        <Box p="18px" bgcolor="white" boxShadow="8px" mx="4px">
            <img style={{height: "120px", width: "120px", backgroundColor: grey[50]}}/>
            <Typography variant="h6">Name</Typography>
            <Typography variant="subtitle1"><span style={{color: green[700]}}>Type</span></Typography>
            <Typography variant="subtitle2">Phone</Typography>
        </Box>
    )
}

export default ListView
