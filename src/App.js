import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import Authenticated from "./Components/Authenticated";
import Dashboard from "./Pages/Dashboard";
import Login from "./Pages/Login";

function App() {
  return (
    <Switch>
      <Route exact path="/" component={Dashboard}/>
        {/* <Authenticated>
          <Dashboard />
        </Authenticated>
      </Route> */}
      <Route exact path="/login" component={Login}/>
        {/* <Authenticated com>
          <Login/>
        </Authenticated>
      </Route> */}
      <Route path="*" render={() => "404 Not Found"} />
    </Switch>
  );
}

export default App;
