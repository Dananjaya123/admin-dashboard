import * as firebase from "firebase/app";


import "firebase/auth";
import "firebase/firestore";


const firebaseConfig = {
    apiKey: "AIzaSyCyaec4kPUdnZxMAVzaIrwvQj2htkZvkcU",
    authDomain: "school-van-tracking-syst-b0111.firebaseapp.com",
    databaseURL: "https://school-van-tracking-syst-b0111.firebaseio.com",
    projectId: "school-van-tracking-syst-b0111",
    storageBucket: "school-van-tracking-syst-b0111.appspot.com",
    messagingSenderId: "431765042746",
    appId: "1:431765042746:web:b0c0c079c69ec1d4cf6703",
    measurementId: "G-0QY1MNZR15"
};

firebase.initializeApp(firebaseConfig);

export const firebaseAuth = firebase.auth();

export const firestore = firebase.firestore();

export default firebase;