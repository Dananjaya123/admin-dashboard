import React, { Component } from "react";
import { Box, Button, CircularProgress, Container, TextField, Typography } from "@material-ui/core";
import logo from "../Assets/Images/logo.jpg";
import {firebaseAuth, firestore} from "../firebase"

class Login extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
            email: "",
            password: "",
            show_Progress: false,
        }

        this.handleChange = this.handleChange.bind();
        this.login = this.login.bind();
    }

    handleChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }


    login = () => {
        let valid_data = true;
        this.state.email_error = null;
        this.state.password_error = null;

        if (this.state.email === "") {
            this.state.email_error = "Required!";
            valid_data = false
        }

        if (this.state.password === "") {
            this.state.password_error = "Required!";
            valid_data = false
        }

        if (valid_data) {
          this.state.show_Progress = true;
        }

        this.setState({
            update: true
        })

        if (valid_data) {
            firestore.collection("users")
            .where('email', '==', this.state.email)
            .where('IsAdmin', '==', true)
            .get()
            .then(querySnapshot => {
              if (!querySnapshot.empty) {
                firebaseAuth.
                signInWithEmailAndPassword(
                  this.state.email,
                  this.state.password
                ).then(res => {
                  this.props.history.replace("/");
                }).catch(err => {
                  if (err.code === 'auth/wrong-password') {
                    this.state.password_error= "Incorrect Password!"
                  }
                  this.setState({
                    show_Progress: false,
                  });
                })
              } else {
                this.state.email_error = "Not Allowed!"
                this.setState({
                  show_Progress: false,
                });
              }
            });
        }
    };
    
  render() {
    return (
      <Container maxWidth="sm">
        <Box
          bgcolor="white"
          borderRadius="5px"
          boxShadow="2"
          textAlign="center"
          p="24px"
          mt="50px"
        >
          <img src={logo} width="100px" />
          <Typography variant="h5" color="textSecondary">
            ADMIN
          </Typography>
          <TextField
            label="Email"
            name="email"
            error={this.state.email_error != null}
            helperText={this.state.email_error}
            onChange={this.handleChange}
            id="outlined-size-small"
            margin="normal"
            variant="outlined"
            fullWidth
            size="small"
          />

          <TextField
            label="Password"
            name="password"
            error={this.state.password_error != null}
            helperText={this.state.password_error}
            onChange={this.handleChange}
            id="outlined-size-small"
            margin="normal"
            type="Password"
            variant="outlined"
            fullWidth
            size="small"
          />
          <br/>
          <br/>
          {this.state.show_Progress ? (
          <CircularProgress size={24} thickness={4} color="primary"/>
          ) : null}
          <br/>
          <Button disableElevation
           variant="contained"
           onClick={this.login} 
           color="primary" 
           fullWidth>
              Login
          </Button>
        </Box>
      </Container>
    );
  }
}

export default Login;
